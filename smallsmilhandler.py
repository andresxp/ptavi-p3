#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        self.Root_layout = ""
        self.Region = ""
        self.Img = ""
        self.Audio = ""
        self.Textstream = ""
        self.Lista = []
        self.contador = 0

    def startElement(self, name, attrs):
        if name == 'root-layout':
            self.Lista.insert(self.contador, "root-layout")
            self.Lista.insert(self.contador + 1, {
                "width": attrs.get("width", ""),
                "height": attrs.get("height", ""),
                "background-color": attrs.get("background-color", "")})
        elif name == 'region':
            self.Lista.insert(self.contador, "region")
            self.Lista.insert(self.contador + 1, {
                "id": attrs.get("id", ""), "top": attrs.get("top", ""),
                "bottom": attrs.get("bottom", ""),
                "left": attrs.get('left', ""),
                "right": attrs.get('right', "")})
        elif name == 'img':
            self.Lista.insert(self.contador, "img")
            self.Lista.insert(self.contador + 1, {
                "src": attrs.get("src", ""), "region": attrs.get("region", ""),
                "begin": attrs.get("begin", ""), "dur": attrs.get('dur', "")})
        elif name == 'audio':
            self.Lista.insert(self.contador, "audio")
            self.Lista.insert(self.contador + 1, {
                "src": attrs.get("src", ""), "begin": attrs.get("begin", ""),
                "dur": attrs.get('dur', "")})
        elif name == 'textstream':
            self.Lista.insert(self.contador, "textstream")
            self.Lista.insert(self.contador + 1, {
                "src": attrs.get("src", ""),
                "region": attrs.get("region", "")})
        self.contador = self.contador + 2

    def get_tags(self):
        return self.Lista


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
