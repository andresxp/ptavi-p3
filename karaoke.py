#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
import smallsmilhandler
import sys
import json
import urllib.request


class KaraokeLocal(ContentHandler):
    def __init__(self, File_shell):
        try:
            parser = make_parser()
            self.cHandler = smallsmilhandler.SmallSMILHandler()
            parser.setContentHandler(self.cHandler)
            parser.parse(open(File_shell))
        except:
            sys.exit("Usage: python3 karaoke.py file.smil")

    def __str__(self):
        n = 0
        lista_final = []
        contador = 0
        while len(self.cHandler.get_tags()) > n:
            if self.cHandler.get_tags()[n] == 'root-layout':
                lista_final.insert(
                    contador, self.cHandler.get_tags()[n] + chr(92) +
                    "twidth=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["width"] +
                    chr(34) + chr(92) + "theight=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["height"] +
                    chr(34) + chr(92) +
                    "tbackground-color=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["background-color"] +
                    chr(34))

            elif self.cHandler.get_tags()[n] == 'region':
                lista_final.insert(
                    contador, self.cHandler.get_tags()[n] +
                    chr(92) +
                    "tid=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["id"] +
                    chr(34) + chr(92) + "ttop=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["top"] +
                    chr(34) + chr(92) +
                    "tbottom=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["bottom"] +
                    chr(34) + chr(92) + "tleft=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["left"] +
                    chr(34) + chr(92) +
                    "tright=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["right"] +
                    chr(34))

            elif self.cHandler.get_tags()[n] == 'img':
                lista_final.insert(
                    contador, self.cHandler.get_tags()[n] +
                    chr(92) +
                    "tsrc=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["src"] +
                    chr(34) + chr(92) + "tregion=" +
                    chr(34) +
                    self.cHandler.get_tags()[n+1]["region"] +
                    chr(34) + chr(92) +
                    "tbegin=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["begin"] +
                    chr(34) + chr(92) + "tdur=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["dur"] +
                    chr(34))

            elif self.cHandler.get_tags()[n] == 'audio':
                lista_final.insert(
                    contador, self.cHandler.get_tags()[n] +
                    chr(92) +
                    "tsrc=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["src"] +
                    chr(34) + chr(92) + "tbegin=" +
                    chr(34) +
                    self.cHandler.get_tags()[n+1]["begin"] +
                    chr(34) + chr(92) +
                    "tdur=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["dur"] +
                    chr(34))
            elif self.cHandler.get_tags()[n] == 'textstream':
                lista_final.insert(
                    contador, self.cHandler.get_tags()[n] +
                    chr(92) +
                    "tsrc=" + chr(34) +
                    self.cHandler.get_tags()[n+1]["src"] +
                    chr(34) + chr(92) + "tregion=" +
                    chr(34) +
                    self.cHandler.get_tags()[n+1]["region"] +
                    chr(34))
            n = n + 2
            contador = contador + 1
        return lista_final

    def to_json(self, file_smile):
        data = {}
        n = 0
        root_layout_First = False
        region_First = False
        img_First = False
        audio_First = False
        textstream_First = False
        while len(self.cHandler.get_tags()) > n:
            if self.cHandler.get_tags()[n] == 'root-layout':
                if not root_layout_First:
                    data['root-layout'] = []
                    root_layout_First = True
                data['root-layout'].append({
                    'width':  self.cHandler.get_tags()[n+1]["width"],
                    'height':  self.cHandler.get_tags()[n+1]["height"],
                    'background-color':
                    self.cHandler.get_tags()[n+1]["background-color"]})
            elif self.cHandler.get_tags()[n] == 'region':
                if not region_First:
                    data['region'] = []
                    region_First = True
                data['region'].append({
                    'id':  self.cHandler.get_tags()[n+1]["id"],
                    'top':  self.cHandler.get_tags()[n+1]["top"],
                    'bottom':  self.cHandler.get_tags()[n+1]["bottom"],
                    'left': self.cHandler.get_tags()[n+1]["left"],
                    'right':  self.cHandler.get_tags()[n+1]["right"]})
            elif self.cHandler.get_tags()[n] == 'img':
                if not img_First:
                    data['img'] = []
                    img_First = True
                data['img'].append({
                    'src':  self.cHandler.get_tags()[n+1]["src"],
                    'region':  self.cHandler.get_tags()[n+1]["region"],
                    'begin':  self.cHandler.get_tags()[n+1]["begin"],
                    'dur':  self.cHandler.get_tags()[n+1]["dur"]})
            elif self.cHandler.get_tags()[n] == 'audio':
                if not audio_First:
                    data['audio'] = []
                    audio_First = True
                data['audio'].append({
                    'src':  self.cHandler.get_tags()[n+1]["src"],
                    'begin':  self.cHandler.get_tags()[n+1]["begin"],
                    'dur':  self.cHandler.get_tags()[n+1]["dur"]})

            elif self.cHandler.get_tags()[n] == 'textstream':
                if not textstream_First:
                    data['textstream'] = []
                    textstream_First = True
                data['textstream'].append({
                    'src':  self.cHandler.get_tags()[n+1]["src"],
                    'region':  self.cHandler.get_tags()[n+1]["region"]})
            n = n + 2

        n = 0
        contador = 0
        First_caracter = False
        longitud_lista = len(file_smile)
        for caracter in file_smile:
            if not First_caracter and caracter == ".":
                First_caracter = True
                contador = n
            n = n + 1
        Nombre_archivo = file_smile[:contador]
        Nombre_file = Nombre_archivo + ".json"
        with open(Nombre_file, 'w') as file:
            json.dump(data, file, indent=4)

if __name__ == "__main__":
    try:
        file = sys.argv[1]
    except:
        sys.exit("Usage: python3 karaoke.py file.smil")
    Informe = KaraokeLocal(file)
    print(Informe.__str__())
    Informe.to_json(file)
